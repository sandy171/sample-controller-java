package io.fabric8.samplecontroller.api.model.v1alpha1;

import io.fabric8.kubernetes.api.model.Namespaced;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.model.annotation.Group;
import io.fabric8.kubernetes.model.annotation.Plural;
import io.fabric8.kubernetes.model.annotation.Version;
import org.apache.flink.kubernetes.operator.api.FlinkDeployment;
import org.apache.flink.kubernetes.operator.api.spec.FlinkDeploymentSpec;
import org.apache.flink.kubernetes.operator.api.status.FlinkDeploymentStatus;

@Version("v1beta1")
@Group("flink.apache.org")
@Plural("flinkdeployments")
public class FlinkCr extends CustomResource<FlinkDeploymentSpec, FlinkDeploymentStatus> implements Namespaced { }
