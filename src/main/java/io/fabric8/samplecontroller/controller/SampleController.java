package io.fabric8.samplecontroller.controller;

import io.fabric8.kubernetes.api.model.HasMetadata;
import io.fabric8.kubernetes.api.model.KubernetesResourceList;
import io.fabric8.kubernetes.api.model.OwnerReference;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.apps.DeploymentBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.client.informers.ResourceEventHandler;
import io.fabric8.kubernetes.client.informers.SharedIndexInformer;
import io.fabric8.kubernetes.client.informers.cache.Cache;
import io.fabric8.kubernetes.client.informers.cache.Lister;
import io.fabric8.samplecontroller.api.model.v1alpha1.FlinkCr;
import org.apache.flink.client.cli.SavepointOptions;
import org.apache.flink.kubernetes.operator.api.spec.FlinkDeploymentSpec;
import org.apache.flink.kubernetes.operator.api.status.FlinkDeploymentStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.sql.Connection;
import java.sql.DriverManager;

public class SampleController {
    private final BlockingQueue<String> workqueue;
    private final SharedIndexInformer<FlinkCr> flinkdeploymentInformer;
    private final SharedIndexInformer<Deployment> deploymentInformer;
    private final Lister<FlinkCr> flinkdeploymentLister;
    private final KubernetesClient kubernetesClient;
    private final MixedOperation<FlinkCr, KubernetesResourceList<FlinkCr>, Resource<FlinkCr>> flinkdeploymentClient;
    public static final Logger logger = LoggerFactory.getLogger(SampleController.class.getSimpleName());

    public SampleController(KubernetesClient kubernetesClient, MixedOperation<FlinkCr, KubernetesResourceList<FlinkCr>, Resource<FlinkCr>> flinkdeploymentClient, SharedIndexInformer<Deployment> deploymentInformer, SharedIndexInformer<FlinkCr> flinkdeploymentInformer, String namespace) {
        this.kubernetesClient = kubernetesClient;
        this.flinkdeploymentClient = flinkdeploymentClient;
        this.flinkdeploymentLister = new Lister<>(flinkdeploymentInformer.getIndexer(), namespace);
        this.flinkdeploymentInformer = flinkdeploymentInformer;
        this.deploymentInformer = deploymentInformer;
        this.workqueue = new ArrayBlockingQueue<>(1024);
        initInformerEventHandlers();
    }

    private void initInformerEventHandlers() {
        // Set up an event handler for when FlinkCr resources change
        flinkdeploymentInformer.addEventHandler(new ResourceEventHandler<FlinkCr>() {
            @Override
            public void onAdd(FlinkCr flinkdeployment) {
                enqueueFlinkCr(flinkdeployment);
            }

            @Override
            public void onUpdate(FlinkCr flinkdeployment, FlinkCr newFlinkCr) {
                enqueueFlinkCr(newFlinkCr);
            }

            @Override
            public void onDelete(FlinkCr flinkdeployment, boolean b) {
                // Do nothing
            }
        });

        // Set up an event handler for when Deployment resources change. This
        // handler will lookup the owner of the given Deployment, and if it is
        // owned by a FlinkCr resource will enqueue that FlinkCr resource for
        // processing. This way, we don't need to implement custom logic for
        // handling Deployment resources. More info on this pattern:
        // https://github.com/kubernetes/community/blob/8cafef897a22026d42f5e5bb3f104febe7e29830/contributors/devel/controllers.md
        deploymentInformer.addEventHandler(new ResourceEventHandler<Deployment>() {
            @Override
            public void onAdd(Deployment deployment) {
                handleObject(deployment);
            }

            @Override
            public void onUpdate(Deployment oldDeployment, Deployment newDeployment) {
                // Periodic resync will send update events for all known Deployments.
                // Two different versions of the same Deployment will always have different RVs.
                if (oldDeployment.getMetadata().getResourceVersion().equals(newDeployment.getMetadata().getResourceVersion())) {
                    return;
                }
                handleObject(newDeployment);
            }

            @Override
            public void onDelete(Deployment deployment, boolean b) {
                handleObject(deployment);
            }
        });
    }

    public void run() {
        logger.info("Starting {} controller", FlinkCr.class.getSimpleName());
        logger.info("Waiting for informer caches to sync");
        while (!deploymentInformer.hasSynced() || !flinkdeploymentInformer.hasSynced()) {
            // Wait till Informer syncs
        }

        //connect to postgres database
        Connection c = null;
        Statement stmt = null;
        if ((c=connect()) != null) {
            logger.info("Connection established");
        }

        while (!Thread.currentThread().isInterrupted()) {
            try {
                logger.info("trying to fetch item from workqueue...");
                if (workqueue.isEmpty()) {
                    logger.info("Work Queue is empty");
                }
                String key = workqueue.take();
                Objects.requireNonNull(key, "key can't be null");
                logger.info("Got {}", key);
                if (key.isEmpty() || (!key.contains("/"))) {
                    logger.warn("invalid resource key: {}", key);
                }

                // Get the FlinkCr resource's name from key which is in format namespace/name
                String name = key.split("/")[1];
                FlinkCr flinkdeployment = flinkdeploymentLister.get(key.split("/")[1]);
                if (flinkdeployment == null) {
                    logger.error("FlinkCr {} in workqueue no longer exists", name);
                    return;
                }


                //Add the resource details to postgres table
                try {
                    String cluster_name = flinkdeployment.getMetadata().getName();
                    String cluster_start_time = flinkdeployment.getMetadata().getCreationTimestamp();
                    String cluster_deletion_time = flinkdeployment.getMetadata().getDeletionTimestamp();
                    String cluster_state = flinkdeployment.getStatus().getJobManagerDeploymentStatus().toString();
                    String last_stable_spec = flinkdeployment.getStatus().getReconciliationStatus().getLastStableSpec();
                    String job_state = flinkdeployment.getStatus().getJobStatus().getState();

                    Map<String, String> jobmanagerLabels = new HashMap<>();
                    jobmanagerLabels.put("app", cluster_name);
                    jobmanagerLabels.put("component", "jobmanager");
                    String jobmanagerLogs = null;
                   try {
                       if (Objects.equals(job_state, "FINISHED")) {
                           jobmanagerLogs = kubernetesClient
                                   .pods()
                                   .withLabels(jobmanagerLabels)
                                   .resources()
                                   .findFirst()
                                   .get()
                                   .getLog(true);
                       }
                   } catch (Exception e){
                       System.out.println("LOGS ERROR: "+ e.getMessage());
                   }


                   //sql query to insert records
                    stmt = c.createStatement();
                    String sql=String.format("INSERT INTO flinkcr VALUES ('%s','%s',%s,'%s','%s',current_timestamp,'%s')", cluster_name,cluster_start_time,cluster_deletion_time,cluster_state,jobmanagerLogs,last_stable_spec);
                    System.out.println(sql);
                    stmt.executeUpdate(sql);
                    //stmt.close();
                    //c.close();
                    //c.commit();
                } catch (Exception e){
                    System.out.println("Insertion Error: "+ e.getMessage());
                    System.exit(0);
                }
                System.out.println("Insertion Successful");


            } catch (InterruptedException interruptedException) {
                Thread.currentThread().interrupt();
                logger.error("controller interrupted..");
            }
        }
    }

    //connecting to postgres database
    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://ec2-15-206-57-2.ap-south-1.compute.amazonaws.com:5432/postgres","postgres","postgres");
            System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }




    private void createDeployments(FlinkCr flinkdeployment) {
        Deployment deployment = createNewDeployment(flinkdeployment);
        kubernetesClient.apps().deployments().inNamespace(flinkdeployment.getMetadata().getNamespace()).resource(deployment).create();
    }

    private void enqueueFlinkCr(FlinkCr flinkdeployment) {
        logger.info("enqueueFlinkCr({})", flinkdeployment.getMetadata().getName());
        String key = Cache.metaNamespaceKeyFunc(flinkdeployment);
        logger.info("Going to enqueue key {}", key);
        if (key != null && !key.isEmpty()) {
            logger.info("Adding item to workqueue");
            workqueue.add(key);
        }
    }

    private void handleObject(HasMetadata obj) {
        logger.info("handleDeploymentObject({})", obj.getMetadata().getName());
        OwnerReference ownerReference = getControllerOf(obj);
        if (ownerReference != null) {
            if (!ownerReference.getKind().equalsIgnoreCase(FlinkCr.class.getSimpleName())) {
                return;
            }
            FlinkCr flinkdeployment = flinkdeploymentLister.get(ownerReference.getName());
            if (flinkdeployment == null) {
                logger.info("ignoring orphaned object '{}' of flinkdeployment '{}'", obj.getMetadata().getSelfLink(), ownerReference.getName());
                return;
            }
            enqueueFlinkCr(flinkdeployment);
        }
    }

    private void updateAvailableReplicasInFlinkCrStatus(FlinkCr flinkdeployment, int replicas) {
        FlinkDeploymentStatus flinkdeploymentStatus = new FlinkDeploymentStatus();
        //flinkdeploymentStatus.setAvailableReplicas(replicas);
        // NEVER modify objects from the store. It's a read-only, local cache.
        // You can create a copy manually and modify it
        FlinkCr flinkdeploymentClone = getFlinkCrClone(flinkdeployment);
        flinkdeploymentClone.setStatus(flinkdeploymentStatus);
        // If the CustomResourceSubresources feature gate is not enabled,
        // we must use Update instead of UpdateStatus to update the Status block of the FlinkCr resource.
        // UpdateStatus will not allow changes to the Spec of the resource,
        // which is ideal for ensuring nothing other than resource status has been updated.
        flinkdeploymentClient.inNamespace(flinkdeployment.getMetadata().getNamespace()).resource(flinkdeploymentClone).replaceStatus();
    }

    /**
     * createNewDeployment creates a new Deployment for a FlinkCr resource. It also sets
     * the appropriate OwnerReferences on the resource so handleObject can discover
     * the FlinkCr resource that 'owns' it.
     * @param flinkdeployment {@link FlinkCr} resource which will be owner of this Deployment
     * @return Deployment object based on this FlinkCr resource
     */
    private Deployment createNewDeployment(FlinkCr flinkdeployment) {
        return new DeploymentBuilder()
                .withNewMetadata()
                  //.withName(flinkdeployment.getSpec().getDeploymentName())
                  .withNamespace(flinkdeployment.getMetadata().getNamespace())
                  .withLabels(getDeploymentLabels(flinkdeployment))
                  .addNewOwnerReference().withController(true).withKind(flinkdeployment.getKind()).withApiVersion(flinkdeployment.getApiVersion()).withName(flinkdeployment.getMetadata().getName()).endOwnerReference()
                .endMetadata()
                .withNewSpec()
                  //.withReplicas(flinkdeployment.getSpec().getReplicas())
                  .withNewSelector()
                  .withMatchLabels(getDeploymentLabels(flinkdeployment))
                  .endSelector()
                  .withNewTemplate()
                     .withNewMetadata().withLabels(getDeploymentLabels(flinkdeployment)).endMetadata()
                     .withNewSpec()
                         .addNewContainer()
                         .withName("nginx")
                         .withImage("nginx:latest")
                         .endContainer()
                     .endSpec()
                  .endTemplate()
                .endSpec()
                .build();
    }

    private Map<String, String> getDeploymentLabels(FlinkCr flinkdeployment) {
        Map<String, String> labels = new HashMap<>();
        labels.put("app", "nginx");
        labels.put("controller", flinkdeployment.getMetadata().getName());
        return labels;
    }

    private OwnerReference getControllerOf(HasMetadata obj) {
        List<OwnerReference> ownerReferences = obj.getMetadata().getOwnerReferences();
        for (OwnerReference ownerReference : ownerReferences) {
            if (ownerReference.getController().equals(Boolean.TRUE)) {
                return ownerReference;
            }
        }
        return null;
    }

    private boolean isControlledBy(HasMetadata obj, FlinkCr flinkdeployment) {
        OwnerReference ownerReference = getControllerOf(obj);
        if (ownerReference != null) {
            return ownerReference.getKind().equals(flinkdeployment.getKind()) && ownerReference.getName().equals(flinkdeployment.getMetadata().getName());
        }
        return false;
    }

    private FlinkCr getFlinkCrClone(FlinkCr flinkdeployment) {
        FlinkCr cloneFlinkCr = new FlinkCr();
        FlinkDeploymentSpec cloneFlinkCrSpec = new FlinkDeploymentSpec();
//        cloneFlinkCrSpec.setDeploymentName(flinkdeployment.getSpec().getDeploymentName());
//        cloneFlinkCrSpec.setReplicas(flinkdeployment.getSpec().getReplicas());

        cloneFlinkCr.setSpec(cloneFlinkCrSpec);
        cloneFlinkCr.setMetadata(flinkdeployment.getMetadata());

        return cloneFlinkCr;
    }
}
